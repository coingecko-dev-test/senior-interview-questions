# Senior Interview Questions
*Please include explanation along with your answers.*

1. Any open source contribution or side projects that you would like to share with us 

2. Why are you interested in the blockchain industry?

3. Tell us about a newer (less than five years old) web technology you like and why?

4. TickerMinutely.where(coin_id: “bitcoin”).where(“timestamp > ?”, 3.months.ago) . Suggestion to improve this performance & bottlenecks.

5. We ran a poll to gather the sentiment of the market from our users. They can cast a vote whether the sentiment is positive or negative. As this feature gets more popular, we are getting complaints from users that their page is stuck indefinitely after they tried to cast a vote. Page response time has spiked and we have to find a way to fix this. Suggest solutions and methods that we can use to overcome this. 

6. What is a "closure" in JavaScript? How does JS Closure works? Can you provide sample code along with your answer?

7. What is Arrow function in JavaScript? How it differs from normal function?

8. You are helping your company to host a web application on Amazon Web Services. As part of designing the architecture to host the application, you have the option to deploy the application in the following manner:-
    * (A) Single availability zone
    * (B) Multiple availability zones in a single region
    * (C) Multi regions

    *Describe which option you would choose and why? Also explain why you did not choose the other options.*

9. Do you require a visa to work in Malaysia?
